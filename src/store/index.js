import { createStore } from '@mpxjs/core'
const store = createStore({
  state: {
    tabBarActive: 'user/user', // 默认底部tab项激活的是主页
    tabBarShow: true, // 控制tabbar是否展示
    statusBarHeight: 45 // 状态栏默认高度
  },
  mutations: {
    CHANGE_TABER(state, tab) { // 底部tab项激活值发生改变
      state.tabBarActive = tab
    },
    setTabBarShow(state, flag) { // 控制tabbar是否展示
      state.tabBarShow = flag
    },
    setStatusBarHeight(state, statusBarHeight) { // 设置状态栏高度
      state.statusBarHeight = statusBarHeight
    }
  }
})
export default store
