// common.js
// 函数防抖：延迟函数执行，多用于input框输入时，显示匹配的输入内容的情况
// 函数节流：单位时间n秒内，第一次触发函数执行 之后不管触发多少次都不执行。到下一个单位时间n秒时 第一次触发函数执行，多用于页面scroll滚动、窗口resize、防止按钮重复点击
// 函数节流是减少函数的触发频率；函数防抖是延迟函数执行，且不管触发多少次都只执行最后一次

/* 函数节流 */
export function throttle(fn, interval) {
  var enterTime = 0 // 触发的时间
  var gapTime = interval || 300 // 间隔时间，如果interval不传值，默认为300ms
  return function () {
    var that = this
    var backTime = new Date() // 第一次函数return即触发的时间
    if (backTime - enterTime > gapTime) {
      fn.call(that, arguments)
      enterTime = backTime // 赋值给第一次触发的时间 保存第二次触发时间
    }
  }
}

/* 函数防抖 */
export function debounce(fn, interval) {
  var timer
  var gapTime = interval || 1000 // 间隔时间 不传值默认为1000ms
  return function () {
    clearTimeout(timer)
    var that = this
    var args = arguments // 保存arguments setTimeout是全局的 arguments不是防抖函数需要的
    timer = setTimeout(function () {
      fn.call(that, args)
    }, gapTime)
  }
}
